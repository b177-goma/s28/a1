/*Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.*/

console.log(fetch('https://jsonplaceholder.typicode.com/todos'));

/*
Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
*/

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

/*Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


/*Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => console.log(response.status));

 /*Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.*/

 fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		userID: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

/*Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different structure',
		Status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

/* Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item'
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

/* Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
